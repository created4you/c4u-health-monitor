<?php

namespace C4U\HealthMonitor;

use C4U\Date\Date;
use C4U\HealthMonitor\Entity\HealthItem;
use C4U\HealthMonitor\Entity\HealthStatus;

class HealthXmlSerialize {

	public function serialize(HealthStatus $healthStatus = null, array $healthItems = null) {
		$root = new \SimpleXMLElement('<healthMonitor/>');
		$this->createHeader($root, $healthStatus, $healthItems);
		$this->createItems($root, $healthItems);
		return $root->asXML();
	}

	private function createHeader(\SimpleXMLElement $root, HealthStatus $healthStatus = null, array $healthItems = null) {
		$status = $root->addChild('status');
		$status->addChild('name', $healthStatus->name);
		$status->addChild('status', $this->getStatus($healthItems));
		$status->addChild('message', $healthStatus->message);
		$status->addChild('lastChange', $healthStatus->lastChange instanceof Date ? $healthStatus->lastChange->toUnixTime() : $healthStatus->lastChange);
	}

	private function createItems(\SimpleXMLElement $root, array $healthItems = null) {
		$items = $root->addChild('items');
		if (!$healthItems) return;
		foreach ($healthItems as $healthItem) {
			$item = $items->addChild('item');
			$item->addChild('name', $healthItem->name);
			$item->addChild('status', $healthItem->status);
			$item->addChild('message', $healthItem->message);
			$item->addChild('lastChange', $healthItem->lastChange instanceof Date ? $healthItem->lastChange->toUnixTime() : $healthItem->lastChange);
		}
	}

	private function getStatus(array $healthItems = null) {
		if (!$healthItems) return HealthStatus::STATUS_ERROR;

		$hasWarning = false;
		$hasError = false;

		/** @var HealthItem $healthItem */
		foreach ($healthItems as $healthItem) {
			if ($healthItem->status == HealthStatus::STATUS_WARNING) $hasWarning = true;
			if ($healthItem->status == HealthStatus::STATUS_ERROR) $hasError = true;
		}
		return $hasError ? HealthStatus::STATUS_ERROR : ($hasWarning ? HealthStatus::STATUS_WARNING : HealthStatus::STATUS_OK);
	}

}
 