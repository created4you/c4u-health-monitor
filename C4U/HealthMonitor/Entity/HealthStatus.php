<?php

namespace C4U\HealthMonitor\Entity;

use C4U\Date\DateFactory;

class HealthStatus {

	const STATUS_OK = 'ok';
	const STATUS_ERROR = 'error';
	const STATUS_WARNING = 'warning';

	public $name;
	public $status;
	public $message;
	public $lastChange;

	public function __construct($name = null, $message = null, $lastChange = null) {
		$this->lastChange = $lastChange == null ? $lastChange = DateFactory::fromUnixTime(time()) : $lastChange;
		$this->message = $message;
		$this->name = $name;
	}

}
 