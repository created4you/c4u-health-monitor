<?php

namespace C4U\HealthMonitor;

use C4U\HealthMonitor\Type\ActiveMonitor;

class DispatchMonitor {

	private $activeMonitors = array();
	private $passiveMonitors = array();

	public function register($class) {
		if ($class instanceof ActiveMonitor) {
			$this->activeMonitors[] = $class;
		}
	}

	public function run() {
		$this->runActiveMonitors();
	}

	private function runActiveMonitors() {
		/** @var ActiveMonitor $activeMonitor */
		foreach ($this->activeMonitors as $activeMonitor) {
			$activeMonitor->run();
		}
	}

}
 