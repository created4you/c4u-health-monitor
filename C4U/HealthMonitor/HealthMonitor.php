<?php

namespace C4U\HealthMonitor;

use C4U\HealthMonitor\Entity\HealthItem;
use C4U\HealthMonitor\Entity\HealthStatus;

class HealthMonitor {

	private $serializator;
	private $deserializator;

	private $status;
	private $monitors = array();

	public function __construct() {
		$this->serializator = new HealthXmlSerialize();
		$this->deserializator = new HealthXmlDeserialize();
	}
	
	public function flush() {
		header("Content-type: text/xml");
		echo $this->serializator->serialize($this->status, $this->monitors);
		exit();
	}

	public function setStatus(HealthStatus $healthStatus) {
		$this->status = $healthStatus;
	}

	public function addMonitor(HealthItem $healthItem) {
		$this->monitors[] = $healthItem;
	}

	public function getMonitor($url) {
		$str = utf8_encode(@file_get_contents($url));
		if (!$str) return false;
		$xml = @simplexml_load_string($str);
		if (!$xml) return false;
		return $this->deserializator->deserialize($xml);
	}

}
 