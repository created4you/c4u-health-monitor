<?php

namespace C4U\HealthMonitor;

use C4U\Date\DateFactory;
use C4U\HealthMonitor\Entity\HealthItem;
use C4U\HealthMonitor\Entity\HealthStatus;

class HealthXmlDeserialize {

	public function deserialize(\SimpleXmlElement $xml) {
		return array(
			'header' => $this->getHeader($xml),
			'items' => $this->getItems($xml),
		);
	}

	private function getHeader(\SimpleXmlElement $xml) {
		/** @var \SimpleXmlElement $header */
		$header = $this->getFirstElement($xml, '/healthMonitor/healthStatus');
		if (!$header) {
			$status = new HealthStatus();
			$status->status = HealthStatus::STATUS_ERROR;
			$status->message = 'Element /healthMonitor/healthStatus not found';
			return $status;
		};
		$status = new HealthStatus();
		$status->status = $this->getFirstElement($header, 'status')->__toString();
		$status->message = $this->getFirstElement($header, 'message')->__toString();
		$status->name = $this->getFirstElement($header, 'name')->__toString();
		$status->lastChange = $this->parseDate($this->getFirstElement($header, 'lastChange')->__toString());
		return $status;
	}

	private function getItems(\SimpleXmlElement $xml) {
		/** @var \SimpleXmlElement $items */
		$items = $xml->xpath('/healthMonitor/healthMonitors/healthMonitor');
		if (!$items) return null;

		$output = array();
		foreach ($items as $item) {
			$itemEl = new HealthItem();
			$itemEl->status = $this->getFirstElement($item, 'status')->__toString();
			$itemEl->message = $this->getFirstElement($item, 'message')->__toString();
			$itemEl->name = $this->getFirstElement($item, 'name')->__toString();
			$itemEl->lastChange = $this->parseDate($this->getFirstElement($item, 'lastChange')->__toString());

			$output[] = $itemEl;
		}
		return $output;
	}

	private function getFirstElement(\SimpleXmlElement $element, $xpath) {
		$found = $element->xpath($xpath);
		if (count($found) <= 0) return null;
		return $found[0];
	}

	private function parseDate($string = null) {
		return $string ? DateFactory::fromUnixTime($string) : null;
	}

}
 