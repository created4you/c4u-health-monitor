<?php

namespace C4U\HealthMonitor\Report;

abstract class ReportStrategy {

	public abstract function filter(array $events);

}
 