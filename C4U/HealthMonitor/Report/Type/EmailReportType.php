<?php

namespace C4U\HealthMonitor\Report\Type;

use C4U\HealthMonitor\Report\ReportType;
use Nette\Latte\Engine;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Templating\FileTemplate;

class EmailReportType extends ReportType {

	private $message;
	private $mailer;
	private $recipient;
	private $hostname;

	public function __construct(
		Message $message,
		IMailer $mailer,
		$recipient,
		$hostname
	) {
		$this->message = $message;
		$this->mailer = $mailer;
		$this->recipient = $recipient;
		$this->hostname = $hostname;
	}

	public function report(array $events, $reportType, $reportStrategy) {
		$template = $this->getTemplate($events, $reportType, $reportStrategy);

		try {
			$this->message
				->setFrom('monitor@created4you.cz')
				->addTo($this->recipient)
				->setSubject('Monitor alert!')
				->setHTMLBody($template);
			$this->mailer->send($this->message);

		} catch (\Exception $e) {
			throw $e;
		}
	}

	public function getTemplate(array $events, $reportType, $reportStrategy) {
		$latte = new FileTemplate();
		$latte->setFile(__DIR__ . '/EmailReportType/template.latte');
		$latte->registerHelperLoader('Nette\Templating\Helpers::loader');
		$latte->registerFilter(new Engine());

		$latte->events = $events;
		$latte->reportType = $reportType;
		$latte->reportStrategy = $reportStrategy;
		$latte->hostname = $this->hostname;

		return $latte->__toString();
	}

}
 