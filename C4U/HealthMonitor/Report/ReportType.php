<?php

namespace C4U\HealthMonitor\Report;

abstract class ReportType {

	public abstract function report(array $events, $reportType, $reportStrategy);

}
 