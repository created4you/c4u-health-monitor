<?php

namespace C4U\HealthMonitor\Report\Strategy;

use C4U\HealthMonitor\Report\ReportStrategy;

class OnEventStrategy extends ReportStrategy {

	public function filter(array $events) {
		return $events;
	}

}
 