<?php

namespace C4U\HealthMonitor\Report\Strategy;

use C4U\HealthMonitor\Entity\HealthEvent;
use C4U\HealthMonitor\Report\ReportStrategy;

class OnErrorStrategy extends ReportStrategy {

	const MODE_ERRORS_ONLY = 'MODE_SHOW_ERRORS_ONLY';
	const MODE_ALL = 'MODE_ALL';

	private $mode = self::MODE_ALL;

	public function setMode($mode) {
		$this->mode = $mode;
	}

	public function filter(array $events) {
		$conditionSatisfied = false;
		$errors = array();
		/** @var HealthEvent $event */
		foreach ($events as $event) {
			if ($event->status == HealthEvent::STATUS_ERROR) {
				$errors[] = $event;
				$conditionSatisfied = true;
			}
		}

		if ($conditionSatisfied) {
			if ($this->mode == self::MODE_ALL) {
				return $events;
			} else if ($this->mode == self::MODE_ERRORS_ONLY) {
				return $errors;
			}
		}

		return false;
	}

}
 