<?php

namespace C4U\HealthMonitor\Type;

use C4U\HealthMonitor\Entity\HealthEvent;
use C4U\HealthMonitor\Report\ReportStrategy;
use C4U\HealthMonitor\Report\ReportType;

interface IMonitor {

	public function logEvent(HealthEvent $event);

	/** @returns ReportType */
	public function getReportType();

	/** @returns ReportStrategy */
	public function getReportStrategy();

}
 