<?php

namespace C4U\HealthMonitor\Type;

use C4U\HealthMonitor\DispatchMonitor;
use C4U\HealthMonitor\Entity\HealthEvent;

abstract class ActiveMonitor implements IMonitor {

	public $events = array();

	public function __construct(
		DispatchMonitor $dispatchMonitor
	) {
		$dispatchMonitor->register($this);
	}

	public function logEvent(HealthEvent $event) {
		$this->events[] = $event;
	}

	public function run() {
		$reportStrategy = $this->getReportStrategy();
		$values = $reportStrategy->filter($this->events);
		if ($values) {
			$this->getReportType()->report($values, __CLASS__, get_class($reportStrategy));
		}
	}

}
 